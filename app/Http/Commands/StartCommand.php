<?php

namespace App\Http\Commands;

use App\Http\Commands\Traits\CustomTraits;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use Telegram;

class StartCommand extends Command
{
    use Traits\CustomMethods;
    use Traits\Globals;

    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $data = $this->getUpdate();
        if ($data->callback_query) $callbackData = $this->parseCallbackData($data);
        $markup = array();

        // $markup
        foreach ($this->commandTexts as $key => $value) {
            $markup[0][] = ['text' => $value, 'callback_data' => 'command-'.$key];
        };

        $text = $this->titleText[$this->name];

        if (isset($callbackData)) {
            Telegram::editMessageText([
                'chat_id' => $data->callback_query->message->chat->id,
                'message_id' => $data->callback_query->message->message_id,
                'text' => $text,    
                'parse_mode' => 'html',
                'reply_markup' => json_encode([
                    'keyboard' =>  $markup,
                    'resize_keyboard' => true
                ]),
            ]);
        } else {
            $this->replyWithMessage([
                'text' => $text,
                'parse_mode' => 'html',
                'reply_markup' => json_encode([
                    'keyboard' =>  $markup,
                    'resize_keyboard' => true
                ]),
            ]);
        }
    }   
}
<?php

namespace App\Http\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

use App\Models\Category;

use Telegram;

class CategoryCommand extends Command
{
    use Traits\CustomMethods;
    use Traits\Globals;

    /**
     * @var string Command Name
     */
    protected $name = "category";

    /**
     * @var string Next Command Name
     */
    protected $nameNext = "category";

    /**
     * @var string Command Description
     */
    protected $description = "Выводит список категорий";

    /**
     * @inheritdoc
     */
    public function handle()
    {   
        $data = $this->getUpdate();
        if ($data->callback_query) $callbackData = $this->parseCallbackData($data);

        $this->maxRowItems = 2;
        $parentId = (isset($callbackData) && isset($callbackData['id'])) ? intval($callbackData['id']) : 0;
        $page = (isset($callbackData) && isset($callbackData['page'])) ? intval($callbackData['page']) : 1;

        // getting data from DB
        $query = Category::where('parent_id', '=', $parentId)->paginate($this->paginationCount[$this->name], ['*'], 'page', $page);

        // IF NO DATA
        if ($query->getCollection('data')->isEmpty()) {
            if ($parentId == 0) {
                $text = $this->emptyText[$this->name];
                if (isset($callbackData)) $markup[$this->markupType][] = $this->createBackButton('command-start');
            } else {
                $this->triggerCommand('product');
                return;
            }
        } else {
            // creating content
            $text = $this->createHeadText($this->titleText[$this->name], $query);
            $markup[$this->markupType][] = array();
            $index = 0;
            foreach ($query->getCollection('data') as $item) {
                $markup[$this->markupType][((isset($markup[$this->markupType][$index]) && count($markup[$this->markupType][$index]) >= ($this->maxRowItems-1))) ? $index++ : $index][] = ['text' => $item->name, 'callback_data' => 'command-category:id-'.$item->id];
            };
            $markup[$this->markupType][] = $this->createPaginationButtons($this->name, $query, $parentId);
            if ($parentId) $markup[$this->markupType][] = $this->createBackButton('command-category:id-'.Category::find($parentId)->parent_id);
        };

        // if in sub category
        if ($parentId) {
            $parentCategories = Category::find($item->id)->parentCategories()->get()->toArray();
            $text .= "\r\n\r\n". implode("  ->  ", $this->getParentCategories($parentCategories[0]));
        };

        // response
        if (isset($callbackData)) {
            Telegram::editMessageText([
                'chat_id' => $data->callback_query->message->chat->id,
                'message_id' => $data->callback_query->message->message_id,
                'text' => $text,
                'parse_mode' => 'html',
                'reply_markup' => (isset($markup) && !empty($markup)) ? json_encode($markup) : false,
            ]);
        } else {
            $this->replyWithMessage([
                'text' => $text,
                'parse_mode' => 'html',
                'reply_markup' => (isset($markup) && !empty($markup)) ? json_encode($markup) : false,
            ]);
        }
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Commands\Traits\CustomTraits;
use Illuminate\Http\Request;

use Telegram;
use App\Models\Category;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\ProductAttributeValue;

class HomeController extends Controller
{
    use \App\Http\Commands\Traits\CustomMethods;
    use \App\Http\Commands\Traits\Globals;

    public function index() {
        return view('welcome');
    }

    public function getUpdates(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $updates = Telegram::commandsHandler(false);

            foreach ($updates as $update) {
                $updateArray = $update->toArray();
                $command = null;
                if (isset($updateArray['callback_query'])) {
                    foreach (explode(":", $updateArray['callback_query']['data']) as $value) {
                        $value = explode("-", $value);
                        $data[$value[0]] = $value[1];
                    }
                    $command = $data['command'];
                } else {
                    $command = array_search($updateArray['message']['text'], $this->commandTexts);
                }
                if ($command) Telegram::triggerCommand($command, $update);
            }

            return response()->json($updates);
        }
    }
}

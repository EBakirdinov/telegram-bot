$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var pending = false;
    // $(document).click(function (e) { 
    //     e.preventDefault();
    //     $.ajax({
    //         url: baseUrl + '/getupdates',
    //         method: 'POST',
    //         dataType: 'json',
    //         beforeSend: function() {
    //             pending = true;
    //         },
    //         complete: function(data) {
    //             pending = false;
    //         },
    //     })
    //     .done(function (response) {
    //         console.log(response);
    //     });
    // });
    setInterval(function () {
        if (!pending) {
            $.ajax({
                url: baseUrl + '/getupdates',
                method: 'POST',
                dataType: 'json',
                beforeSend: function() {
                    pending = true;
                },
                complete: function(data) {
                    pending = false;
                },
            })
            .done(function (response) {
                console.log(response);
            });
        }
    }, 1);
});